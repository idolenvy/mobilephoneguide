//
//  ListPresenterTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble
import Result

@testable import MobilePhoneGuide

class ListPresenterTests: QuickSpec {
  
  override func spec() {
    
    var presenter: ListPresenter!
    var viewController: MockListViewController!
    
    beforeEach {
      presenter = ListPresenter()
      viewController = MockListViewController()
      presenter.viewController = viewController
    }
    
    describe("Display mobile data from webservice") {
      context("Send response result to viewController") {
        
        it("displaySelectMobile should be trigger") {
          presenter.presentMobileDataFromService(response: List.GetMobileData.Response(result: .success([Mobile()])))
          expect(viewController.mobiles.count) == 1
        }
      }
    }
    
    describe("Display mobile from Sort") {
      context("Send response result to viewController") {
        
        it("displayMobileListWithSort should be trigger with data = 2") {
          presenter.presentMobileWithSortList(response: List.GetMobileList.Response(mobiles: [Mobile(), Mobile()]))
          expect(viewController.mobiles.count) == 2
        }
      }
    }
    
    describe("Select Mobile") {
      context("Select one Mobile") {
        it("displaySelectMobile should be trigger") {
          presenter.presentSelectMobile(response: List.SelectMobile.Response())
          expect(viewController.isSelectMobileCall) == true
        }
      }
    }
    
  }
  
}


class MockListViewController: ListDisplayLogic {
  var isSelectMobileCall = false
  var mobiles: [List.DisplayMobile] = []
  
  func displayMobileData(viewModel: List.GetMobileData.ViewModel) {
    switch viewModel.result {
      case .success(let mobiles):
      self.mobiles = mobiles
      default: break
    }
  }
  
  func displayMobileListWithSort(viewModel: List.GetMobileList.ViewModel) {
    mobiles = viewModel.mobiles
  }
  
  func displaySelectMobile(viewModel: List.SelectMobile.ViewModel) {
    isSelectMobileCall = true
  }
}
