//
//  ListInteractorTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble
import Moya

@testable import MobilePhoneGuide

class ListInteractorTests: QuickSpec {
  
  override func spec() {
    var interactor: ListInteractor!
    var presenter: MockListPresenter!
    
    beforeEach {
      interactor = ListInteractor()
      presenter = MockListPresenter()
      interactor.presenter = presenter
    }
    
    describe("GetMobileDataFromService") {
      context("Success") {
        beforeEach {
          let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
          APIWorker.shared.provider = provider
        }
        
        it("Should get 1 sampleData from Stub") {
          interactor.getMobileDataFromService(request: List.GetMobileData.Request())
          expect(presenter.isSuccess) == true
          expect(presenter.mobiles.count) == 1
        }
      }
      
      context("Backend return empty data") {
        beforeEach {
          let provider: MoyaProvider<APIService> = self.noDataStubbingProvider()
          APIWorker.shared.provider = provider
        }
        
        it("Should fail & get 0 mobile data") {
          interactor.getMobileDataFromService(request: List.GetMobileData.Request())
          expect(presenter.error) == List.GetMobileData.ViewModel.GetMobileError.noData
        }
      }
      
      context("Can't connect to backend") {
        beforeEach {
          let provider: MoyaProvider<APIService> = self.errorStubbingProvider()
          APIWorker.shared.provider = provider
        }
        
        it("Should fail with webService type") {
          interactor.getMobileDataFromService(request: List.GetMobileData.Request())
          expect(presenter.error) == List.GetMobileData.ViewModel.GetMobileError.webService
        }
      }
      
    }
    
    describe("Get MobileList with sorting") {
      beforeEach {
        let mobile1 = Mobile()
        mobile1.id = 1
        mobile1.name = "iPhone XX"
        mobile1.price = 100.0
        mobile1.rating = 5.0
        
        let mobile2 = Mobile()
        mobile2.id = 2
        mobile2.name = "iPhone YY"
        mobile2.price = 200.0
        mobile2.rating = 4.0
        
        let mobile3 = Mobile()
        mobile3.id = 3
        mobile3.name = "iPhone ZZ"
        mobile3.price = 300.0
        mobile3.rating = 4.5
        
        interactor.mobiles = [mobile1, mobile2, mobile3]
        
        let favouriteWorker = FavouriteWorker.shared
        favouriteWorker.addFavourite(with: 1)
        favouriteWorker.addFavourite(with: 2)
        favouriteWorker.addFavourite(with: 3)
      }
      
      context("Sort by Price low to high") {
        it("Should order with iPhone XX , iPhone YY, iPhone ZZ") {
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .normal, sortType: .lowToHigh))
          expect(presenter.mobiles.first?.name) == "iPhone XX"
          expect(presenter.mobiles[1].name) == "iPhone YY"
          expect(presenter.mobiles.last?.name) == "iPhone ZZ"
          
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .favourite, sortType: .lowToHigh))
          expect(presenter.mobiles.first?.name) == "iPhone XX"
          expect(presenter.mobiles[1].name) == "iPhone YY"
          expect(presenter.mobiles.last?.name) == "iPhone ZZ"
        }
      }
      
      context("Sort by Price high to low") {
        it("Should order with iPhone ZZ , iPhone YY, iPhone XX") {
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .normal, sortType: .highToLow))
          expect(presenter.mobiles.first?.name) == "iPhone ZZ"
          expect(presenter.mobiles[1].name) == "iPhone YY"
          expect(presenter.mobiles.last?.name) == "iPhone XX"
          
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .favourite, sortType: .highToLow))
          expect(presenter.mobiles.first?.name) == "iPhone ZZ"
          expect(presenter.mobiles[1].name) == "iPhone YY"
          expect(presenter.mobiles.last?.name) == "iPhone XX"
        }
      }
      
      context("Sort by Rating") {
        it("Should order with iPhone XX , iPhone ZZ, iPhone YY") {
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .normal, sortType: .rating))
          expect(presenter.mobiles.first?.name) == "iPhone XX"
          expect(presenter.mobiles[1].name) == "iPhone ZZ"
          expect(presenter.mobiles.last?.name) == "iPhone YY"
          
          interactor.getMobileList(request: List.GetMobileList.Request(tabType: .favourite, sortType: .rating))
          expect(presenter.mobiles.first?.name) == "iPhone XX"
          expect(presenter.mobiles[1].name) == "iPhone ZZ"
          expect(presenter.mobiles.last?.name) == "iPhone YY"
        }
      }
      
    }
    
    
    describe("Select Mobile") {
      context("Select one mobile") {
        beforeEach {
            let mobile1 = Mobile()
            mobile1.id = 1
            mobile1.name = "iPhone XX"
            mobile1.price = 100.0
            mobile1.rating = 5.0
            
            let mobile2 = Mobile()
            mobile2.id = 2
            mobile2.name = "iPhone YY"
            mobile2.price = 200.0
            mobile2.rating = 4.0
            
            let mobile3 = Mobile()
            mobile3.id = 3
            mobile3.name = "iPhone ZZ"
            mobile3.price = 300.0
            mobile3.rating = 4.5
            
            interactor.mobiles = [mobile1, mobile2, mobile3]
        }
        
        it("presentSelectMobile should be call") {
          interactor.selectMobile(request: List.SelectMobile.Request(id: 1))
          expect(presenter.isSelectMobileCall) == true
        }
        
      }
    }
  }
  
}


class MockListPresenter: ListPresentationLogic {
  var isSuccess = false
  var mobiles: [Mobile] = []
  var error: List.GetMobileData.ViewModel.GetMobileError = .noData
  var selectMobile = Mobile()
  
  var isSelectMobileCall = false
  
  func presentMobileDataFromService(response: List.GetMobileData.Response) {
    switch response.result {
    case .success(let mobiles):
      isSuccess = true
      self.mobiles = mobiles
    case .failure(.noData):
      error = .noData
    case .failure(.webService):
      error = .webService
    default: break
    }
  }
  
  func presentMobileWithSortList(response: List.GetMobileList.Response) {
    mobiles = response.mobiles
  }
  
  func presentSelectMobile(response: List.SelectMobile.Response) {
    isSelectMobileCall = true
  }
}

extension ListInteractorTests {
  func noDataEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(200, """
                                    []
                                    """.utf8Encoded)
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func noDataStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: noDataEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
  
  func errorEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(500, Data())
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func errorStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: errorEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
}


