//
//  APIWorkerTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble
import Moya

@testable import MobilePhoneGuide

class APIWorkerTests: QuickSpec {
  
  override func spec() {
    
    describe("APIWorker") {
      context("GetMobileList with various response") {
        
        it("Success") {
          let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
          APIWorker.shared.provider = provider
          APIWorker.shared.getMobileList(completion: { result in
            switch result {
            case .success(let mobiles):
              expect(mobiles.count) == 1
            default: break
            }
          })
        }
        
        it("Error with cannot connect to BE code 500") {
          APIWorker.shared.provider = self.errorStubbingProvider()
          APIWorker.shared.getMobileList(completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.webService
            default: break
            }
          })
        }
        
        it("Get response back but noData") {
          APIWorker.shared.provider = self.noDataStubbingProvider()
          APIWorker.shared.getMobileList(completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.noData
            default: break
            }
          })
        }
        
        it("Request and return failure should equal webService") {
          APIWorker.shared.provider = self.requestErrorStubbingProvider()
          APIWorker.shared.getMobileList(completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.webService
            default: break
            }
          })
        }
        
      }
      
      context("GetMobileImages with various response") {
        
        it("Success") {
          let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
          APIWorker.shared.provider = provider
          APIWorker.shared.getMobileImages(with: 1, completion: { result in
            switch result {
            case .success(let mobileImages):
              expect(mobileImages.count) == 3
            default: break
            }
          })
        }
        
        it("Error with cannot connect to BE code 500") {
          APIWorker.shared.provider = self.errorStubbingProvider()
          APIWorker.shared.getMobileImages(with: 1, completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.webService
            default: break
            }
          })
        }
        
        it("Get response back but noData") {
          APIWorker.shared.provider = self.noDataStubbingProvider()
          APIWorker.shared.getMobileImages(with: 1, completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.noData
            default: break
            }
          })
        }
        
        it("Request error get failure webService") {
          APIWorker.shared.provider = self.requestErrorStubbingProvider()
          APIWorker.shared.getMobileImages(with: 1, completion: { result in
            switch result {
            case .failure(let error):
              expect(error) == APIError.webService
            default: break
            }
          })
        }
        
      }
      
    }
  }
  
  
}

extension APIWorkerTests {
  func noDataEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(200, """
                                    []
                                    """.utf8Encoded)
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func noDataStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: noDataEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
  
  func errorEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(500, Data())
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func errorStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: errorEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
  
  public func requestErrorStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(requestClosure: { (endpoint: Endpoint, resultClosure: MoyaProvider.RequestResultClosure) in
      resultClosure(.failure(.underlying(NSError(domain: "", code: 789, userInfo: nil), nil)))
    }, stubClosure: MoyaProvider.immediatelyStub)
  }
}
