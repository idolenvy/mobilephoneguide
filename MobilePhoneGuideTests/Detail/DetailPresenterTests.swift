//
//  DetailPresenterTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/23/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble

@testable import MobilePhoneGuide

class DetailPresenterTests: QuickSpec {
  
  override func spec() {
    
    var presenter: DetailPresenter!
    var viewController: MockDetailViewController!
    
    beforeEach {
      presenter = DetailPresenter()
      viewController = MockDetailViewController()
      presenter.viewController = viewController
    }
    
    describe("Display select data") {
      context("Should present correct data") {
        
        let mobile = Mobile()
        mobile.name = "iPhone XYZ"
        mobile.id = 1

        let gallery1 = MobileImage()
        gallery1.url = "thumbnailTest1"
        
        let gallery2 = MobileImage()
        gallery2.url = "thumbnailTest2"
        
        let galleries = [gallery1, gallery2]
        
        it("Should display correct data") {
          presenter.presentDetail(response: Detail.GetDetail.Response(mobile: mobile, gallery: galleries))
          expect(viewController.mobileDetail?.mobile.name) == "iPhone XYZ"
          expect(viewController.mobileDetail?.mobile.id) == 1
          expect(viewController.mobileDetail?.gallery.first?.url) == "thumbnailTest1"
          expect(viewController.mobileDetail?.gallery.last?.url) == "thumbnailTest2"
        }
      }
    }
    
  }
  
}


class MockDetailViewController: DetailDisplayLogic {
  var mobileDetail: Detail.GetDetail.ViewModel.MobileDetail?
  func displayDetail(viewModel: Detail.GetDetail.ViewModel) {
    mobileDetail = viewModel.mobileDetail
  }
}
