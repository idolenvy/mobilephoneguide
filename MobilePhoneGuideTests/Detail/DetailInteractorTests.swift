//
//  DetailInteractorTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble
import Moya


@testable import MobilePhoneGuide

class DetailInteractorTests: QuickSpec {
  
  override func spec() {
    var interactor: DetailInteractor!
    var presenter: MokcDetailPresenter!
    
    beforeEach {
      interactor = DetailInteractor()
      presenter = MokcDetailPresenter()
      interactor.presenter = presenter
    }
    
    describe("GetDetailData") {
      context("Send select data to presenter") {
        
        beforeEach {
          let mobile = Mobile()
          mobile.id = 0
          mobile.name = "iPhone XYZ"
          mobile.thumbImageURL = "thumbNailImageTest"
          interactor.selectMobile = mobile
        }
        
        it("Success") {
          let provider: MoyaProvider<APIService> = MoyaProvider<APIService>(stubClosure: MoyaProvider.immediatelyStub)
          APIWorker.shared.provider = provider
          
          interactor.getDetailData(request: Detail.GetDetail.Request())
          expect(presenter.mobile?.name) == "iPhone XYZ"
          expect(presenter.gallery?.count) == 3
        }
        
        it("noData from backend use thumbNail from mobile as gallery") {
          let provider: MoyaProvider<APIService> = self.noDataStubbingProvider()
          APIWorker.shared.provider = provider
          interactor.getDetailData(request: Detail.GetDetail.Request())
          expect(presenter.mobile?.name) == "iPhone XYZ"
          expect(presenter.gallery?.count) == 1
          expect(presenter.gallery?.first?.url) == "thumbNailImageTest"
        }
        
        it("Can't connect to backend use thumbNail from mobile as gallery") {
          let provider: MoyaProvider<APIService> = self.errorStubbingProvider()
          APIWorker.shared.provider = provider
          interactor.getDetailData(request: Detail.GetDetail.Request())
          expect(presenter.mobile?.name) == "iPhone XYZ"
          expect(presenter.gallery?.count) == 1
          expect(presenter.gallery?.first?.url) == "thumbNailImageTest"
        }
        
      }
    }
    
  }
  
}

class MokcDetailPresenter: DetailPresentationLogic {
  var mobile: Mobile?
  var gallery: [MobileImage]?
  
  func presentDetail(response: Detail.GetDetail.Response) {
    self.mobile = response.mobile
    self.gallery = response.gallery
  }
}

extension DetailInteractorTests {
  func noDataEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(200, """
                                    []
                                    """.utf8Encoded)
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func noDataStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: noDataEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
  
  func errorEndpointsClosure(target: APIService) -> Endpoint {
    let sampleResponseClosure = { () -> EndpointSampleResponse in
      return .networkResponse(500, Data())
    }
    
    let url = target.baseURL.absoluteString
    let method = target.method
    let endpoint = Endpoint(url: url, sampleResponseClosure: sampleResponseClosure, method: method, task: target.task, httpHeaderFields: target.headers)
    return endpoint
  }
  
  public func errorStubbingProvider() -> MoyaProvider<APIService> {
    return MoyaProvider<APIService>(endpointClosure: errorEndpointsClosure, stubClosure: MoyaProvider.immediatelyStub)
  }
}
