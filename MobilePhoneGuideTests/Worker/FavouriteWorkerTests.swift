//
//  FavouriteWorkerTests.swift
//  MobilePhoneGuideTests
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Quick
import Nimble

@testable import MobilePhoneGuide

class FavouriteWorkerTests: QuickSpec {
  
  override func spec() {
    
    describe("FavouriteWorker") {
      let favouriteWithItemKey = "favouriteWithItemKey"
      let worker = FavouriteWorker.shared
      worker.favouriteKey = favouriteWithItemKey
      
      beforeEach {
        worker.defaults.set(nil, forKey: favouriteWithItemKey)
      }
      
      context("LoadFavourite") {
        it("No favourite - should return empty array") {
          expect(FavouriteWorker.shared.loadFavourite().count) == 0
        }
        
        it("Save 1 favourite into worker - should return 1 favourite item") {
          worker.addFavourite(with: 1)
          expect(worker.loadFavourite().count) == 1
        }
      }
      
      context("GetIsFavourite") {
        it("With empty favourite data will get any mobileId is not favourite") {
          expect(worker.getIsFavouriteById(mobileId: 1)) == false
          expect(worker.getIsFavouriteById(mobileId: 2)) == false
          expect(worker.getIsFavouriteById(mobileId: 999)) == false
        }
        
        it("Add mobileId 1 into favourite data will get 1 as mobileId favourite") {
          worker.addFavourite(with: 1)
          expect(worker.getIsFavouriteById(mobileId: 1)) == true
        }
      }
      
      context("DeleteFavourite") {
        beforeEach {
          worker.addFavourite(with: 1)
          worker.addFavourite(with: 2)
          worker.addFavourite(with: 3)
        }
        
        it("Try delete 2 out from favourite so favourite should not have 2 and count should be 2") {
          worker.deleteFavourite(with: 2)
          expect(worker.loadFavourite().contains(2)) == false
          expect(worker.loadFavourite().count) == 2
        }
        
        it("Try delete all favourite should have 0 favourite") {
          worker.deleteFavourite(with: 1)
          worker.deleteFavourite(with: 2)
          worker.deleteFavourite(with: 3)
          expect(worker.loadFavourite().count) == 0
        }
      }
      
    }
    
  }
}
