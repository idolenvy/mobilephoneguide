//
//  ListViewController+TableView.swift
//  MobilePhoneGuide
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import UIKit
import Foundation

extension ListViewController: UITableViewDataSource {
  func setUpTableView() {
    tableView?.estimatedRowHeight = 120.0
    tableView?.rowHeight = 120.0
  }
  
  func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
    return 120.0
  }
  
  func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    return mobiles.count
  }
  
  func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
    if tabType == .favourite {
      return true
    } else {
      return false
    }
  }
  
  func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
    if tabType == .favourite {
      let delete = UITableViewRowAction(style: .destructive, title: "delete") { (action, indexPath) in
        if let mobile = self.mobiles[indexPath.row] as? List.DisplayMobile {
          FavouriteWorker.shared.deleteFavourite(with: mobile.id)
          self.getMobileList()
        }
      }
      return [delete]
    } else {
      return nil
    }
  }
  
  func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
    guard let cell = tableView.dequeueReusableCell(withIdentifier: "ListTableViewCell",
                                                   for: indexPath) as? ListTableViewCell else {
                                                    return UITableViewCell()
    }
    
    guard let mobile = mobiles[indexPath.row] as? List.DisplayMobile else { return cell }
    cell.onFavouriteClick = {
      FavouriteWorker.shared.addFavourite(with: mobile.id)
      self.getMobileList()
    }
    
    cell.setMobile(mobile: mobile, tabType: tabType)
    
    return cell
  }
}

extension ListViewController: UITableViewDelegate {
  func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
    tableView.deselectRow(at: indexPath, animated: true)
    
    guard let mobile = mobiles[indexPath.row] as? List.DisplayMobile else { return }
    selectMobile(mobile: mobile)
  }
}
