//
//  DetailViewController+Carousel.swift
//  MobilePhoneGuide
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import UIKit
import iCarousel
import SDWebImage

extension DetailViewController: iCarouselDataSource {
  func numberOfItems(in carousel: iCarousel) -> Int {
    guard let count = mobileDetail?.gallery.count else { return 0 }
    return count
  }
  
  func carousel(_ carousel: iCarousel, viewForItemAt index: Int, reusing view: UIView?) -> UIView {
    let imageSize = carousel.frame.size.height
    let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: imageSize, height: imageSize))
    imageView.contentMode = .scaleAspectFit
    guard let imageData = mobileDetail?.gallery[index] else {
      return imageView
    }
    
    if !imageData.url.hasPrefix("http") { //Fix for unsupport url
      let urlWithHttp = "http://\(imageData.url)"
      imageView.sd_setImage(with: URL(string: urlWithHttp),
                            placeholderImage: #imageLiteral(resourceName: "default"),
                            options: SDWebImageOptions.retryFailed,
                            completed: nil)

    } else {
      imageView.sd_setImage(with: URL(string: imageData.url),
                                placeholderImage: #imageLiteral(resourceName: "default"),
                                options: SDWebImageOptions.retryFailed,
                                completed: nil)
    }
    
    return imageView
  }
  
  
}

extension DetailViewController: iCarouselDelegate {
  
  func carousel(_ carousel: iCarousel, valueFor option: iCarouselOption, withDefault value: CGFloat) -> CGFloat {
    if (option == .spacing) {
      return value * 1.1
    }
    return value
  }
}

