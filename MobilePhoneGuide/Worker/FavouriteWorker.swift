//
//  FavouriteWorker.swift
//  MobilePhoneGuide
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Foundation

class FavouriteWorker {
  public static private(set) var shared = FavouriteWorker()
  var favouriteKey = "FavouriteItems"
  let defaults = UserDefaults.standard
  
  func loadFavourite() -> [Int] {
    guard let favouriteItems = defaults.array(forKey: favouriteKey) as? [Int] else { return [] }
    return favouriteItems
  }
  
  func getIsFavouriteById(mobileId: Int) -> Bool {
    if loadFavourite().contains(mobileId) {
      return true
    } else {
      return false
    }
  }

  func addFavourite(with mobileId: Int) {
    var favouriteItems = loadFavourite()
    if !favouriteItems.contains(mobileId) {
      favouriteItems.append(mobileId)
    }
    defaults.set(favouriteItems, forKey: favouriteKey)
  }
  
  func deleteFavourite(with mobileId: Int) {
    var favouriteItems = loadFavourite()
    if let index = favouriteItems.index(of: mobileId) {
      favouriteItems.remove(at: index)
    }
    defaults.set(favouriteItems, forKey: favouriteKey)
  }
}
