//
//  String+Contain.swift
//  NewsFeed
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright (c) 2018 IdolEnvy. All rights reserved.

import Foundation

extension String {
  var utf8Encoded: Data {
    return data(using: .utf8)!
  }
}
