//
//  APIService.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation
import Moya
import Alamofire

public enum APIService {
  case getMobileList()
  case getMobileImage(mobileId: Int)
}

extension APIService: TargetType {
  public var baseURL: URL {
    return URL(string: "https://scb-test-mobile.herokuapp.com/")!
  }
  
  public var method: Moya.Method {
    return .get
  }
  
  public var path: String {
    switch self {
    case .getMobileList:
      return "api/mobiles/"
    case .getMobileImage(let mobileId):
      return "api/mobiles/\(mobileId)/images/"
    }
  }
  
  public var sampleData: Data {
    switch self {
    case .getMobileList:
      return """
      [{"name":"Moto E4 Plus","description":"First place in our list goes to the excellent Moto E4 Plus. It's a cheap phone that features phenomenal battery life, a fingerprint scanner and a premium feel design, plus it's a lot cheaper than the Moto G5 below. It is a little limited with its power, but it makes up for it by being able to last for a whole two days from a single charge. If price and battery are the most important features for you, the Moto E4 Plus will suit you perfectly.","thumbImageURL":"https://cdn.mos.cms.futurecdn.net/grwJkAGWQp4EPpWA3ys3YC-650-80.jpg","brand":"Samsung","id":1,"price":179.99,"rating":4.9}]
      """.utf8Encoded
    case .getMobileImage:
      return """
      [{"id":1,"url":"https://www.91-img.com/gallery_images_uploads/f/c/fc3fba717874d64cf15d30e77a16617a1e63cc0b.jpg","mobile_id":1},{"id":6,"url":"https://www.91-img.com/gallery_images_uploads/b/4/b493185e7767c2a99cfeef712b11377f625766f2.jpg","mobile_id":1},{"id":7,"url":"https://www.91-img.com/gallery_images_uploads/c/3/c32cff8945621ad06c929f50af9f7c55f978c726.jpg","mobile_id":1}]
      """.utf8Encoded
    }
  }
  
  public var parameters: [String: Any]? {
    return [:]
  }
  
  public var task: Task {
    let encoding: ParameterEncoding = method == .get ? Alamofire.URLEncoding.default : Alamofire.JSONEncoding.default
    return Task.requestParameters(parameters: parameters!, encoding: encoding)
  }
  
  public var headers: [String : String]? {
    return nil
  }
}
