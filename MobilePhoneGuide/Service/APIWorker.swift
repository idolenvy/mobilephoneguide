//
//  APIWorker.swift
//  NewsFeed
//
//  Created by Idol Envy on 9/4/2561 BE.
//  Copyright © 2561 IdolTIMEs. All rights reserved.
//

import Foundation
import Moya
import Result

public enum APIError: Swift.Error {
  case noData
  case noConnection
  case webService
}

public class APIWorker {
  public static private(set) var shared = APIWorker()
  
  var provider: MoyaProvider<APIService>
  
  public init(provider: MoyaProvider<APIService> = MoyaProvider<APIService>()) {
    self.provider = provider
  }
  
  public func getMobileList(completion: @escaping (Result<[Mobile], APIError>) -> Void) {
    
    if !Reachability.isConnectedToNetwork() {
      completion(.failure(.noConnection))
      return
    }
    
    provider.request(.getMobileList()) { result in
      switch result {
        case .success(let moyaResponse):
          let statusCode = moyaResponse.statusCode
          guard statusCode == 200,
            let mobiles = try? JSONDecoder().decode([Mobile].self, from: moyaResponse.data) else {
              completion(.failure(.webService))
              return
          }
          
          guard mobiles.count != 0 else {
            completion(.failure(.noData))
            return
          }
          
          completion(.success(mobiles))
        case .failure:
          completion(.failure(.webService))
      }
    }
  }
  
  public func getMobileImages(with mobileId: Int,
                              completion: @escaping (Result<[MobileImage], APIError>) -> Void) {
    
    if !Reachability.isConnectedToNetwork() {
      completion(.failure(.noConnection))
      return
    }
    
    provider.request(.getMobileImage(mobileId: mobileId)) { result in
      switch result {
      case .success(let moyaResponse):
        let statusCode = moyaResponse.statusCode
        guard statusCode == 200,
          let mobileImages = try? JSONDecoder().decode([MobileImage].self, from: moyaResponse.data) else {
            completion(.failure(.webService))
            return
        }
        
        guard mobileImages.count != 0 else {
          completion(.failure(.noData))
          return
        }
        
        completion(.success(mobileImages))
      case .failure:
        completion(.failure(.webService))
      }
    }
  }
}
