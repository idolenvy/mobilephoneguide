//
//  MobileImages.swift
//  MobilePhoneGuide
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Foundation

public class MobileImage: Decodable {
  var id: Int
  var url: String
  var mobileId: Int
  
  init() {
    id = 0
    url = ""
    mobileId = 0
  }
  
  private enum CodingKeys: String, CodingKey {
    case id
    case url
    case mobileId = "mobile_id"
  }
}
