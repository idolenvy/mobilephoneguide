//
//  Mobile.swift
//  MobilePhoneGuide
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright © 2018 IdolEnvy. All rights reserved.
//

import Foundation

public class Mobile: Decodable {
  var id: Int
  var rating: Float
  var name: String
  var description: String
  var thumbImageURL: String
  var brand: String
  var price: Float
  
  init() {
    id = 0
    rating = 0.0
    name = ""
    description = ""
    thumbImageURL = ""
    brand = ""
    price = 0.0
  }
}
