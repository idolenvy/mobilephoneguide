//
//  ListTableViewCell.swift
//  mobilesFeed
//
//  Created by IdolEnvy on 4/22/18.
//  Copyright (c) 2018 IdolEnvy. All rights reserved.

import UIKit
import SDWebImage

class ListTableViewCell: UITableViewCell {
  var onFavouriteClick : (() -> Void)? = nil
  
  @IBOutlet var mobilesImage: UIImageView!
  @IBOutlet var mobilesTitle: UILabel!
  @IBOutlet var mobilesDescription: UILabel!
  @IBOutlet var mobilesRating: UILabel!
  @IBOutlet var mobilesPrice: UILabel!
  
  @IBOutlet var favouriteButton: UIButton!
  
  func setMobile(mobile: List.DisplayMobile, tabType: TabType) {
    mobilesImage?.sd_setImage(with: URL(string: mobile.thumbImageURL),
                          placeholderImage: UIImage(named: "default"),
                          options: .refreshCached,
                          completed: nil)
    
    mobilesTitle.text = mobile.name
    mobilesDescription.text = mobile.description
    mobilesPrice.text = "$\(mobile.price)"
    mobilesRating.text = "\(mobile.rating)"
    
    if tabType == .favourite {
      favouriteButton.isHidden = true
    } else {
      favouriteButton.isHidden = false
    }
    
    if FavouriteWorker.shared.getIsFavouriteById(mobileId: mobile.id) {
      favouriteButton.setImage(#imageLiteral(resourceName: "icon_favourite_select"), for: .normal)
    } else {
      favouriteButton.setImage(#imageLiteral(resourceName: "icon_favourite_unselect"), for: .normal)
    }
  }
  
  @IBAction func favoriteClicked(sender: UIButton) {
    if let onFavouriteClick = self.onFavouriteClick {
      onFavouriteClick()
    }
  }
}
